package com.jedicate.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jedicate.models.Category;
import com.jedicate.sumireapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Trompetica on 1/25/2018.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>{
    private List<Category> categoryList;
    private LayoutInflater inflater;
    private Context context;
    public DataTransferInterface data;
    private static final String LOG_TAG = CategoryAdapter.class.getName();

    public CategoryAdapter(Context context, List<Category> categoryList, DataTransferInterface data) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.categoryList = categoryList;
        this.data = data;
    }
    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = inflater.inflate(R.layout.list_category_item, parent, false);
        CategoryViewHolder holder = new CategoryViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        final Category currentCategory = categoryList.get(position);
        holder.cat_name.setText(currentCategory.getName());
        final String currentCatId = currentCategory.getId();

        //get thumbnail
        String image = currentCategory.getThumb_medium();
        Uri uri = Uri.parse(image);
        if (image != null && !image.isEmpty()) {
            Picasso.with(context)
                    .load(uri)
                    .into(holder.cat_thumb);
        } else {
            holder.cat_thumb.setImageResource(R.drawable.no_image);
        }
        //go to link on item click
        holder.cat_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.getCategoryId(currentCatId);
            }
        });
    }

    public void addAll(List<Category> data) {
        categoryList.clear();
        categoryList.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        int items = this.categoryList.size();
        return items;
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.category_name)
        TextView cat_name;
        @BindView(R.id.cat_imageView)
        ImageView cat_thumb;
        @BindView(R.id.cat_layout)
        View cat_layout;

        CategoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface DataTransferInterface {
        void getCategoryId(String catId);
    }
}
