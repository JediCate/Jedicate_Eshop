package com.jedicate.adapters;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jedicate.fragments.ProductPresentationFragment;
import com.jedicate.models.Product;
import com.jedicate.sumireapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Trompetica on 1/22/2018.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private List<Product> productsList;
    private LayoutInflater inflater;
    private Context context;
    private static final String LOG_TAG = ProductsAdapter.class.getName();

    public ProductsAdapter(Context context, List<Product> productList) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.productsList = productList;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = inflater.inflate(R.layout.list_product_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Product currentArticle = productsList.get(position);
        final String name = currentArticle.getName();
        holder.name.setText(name);
        holder.product_id.setText("Id: " + currentArticle.getProduct_id());

        final String price = currentArticle.getPrice();
        holder.price.setText(price);

        final String stock = currentArticle.getStock();
        if(stock.equals("In Stock")){
            holder.stock.setTextColor(Color.parseColor("#0C9D10"));
        }else{
            holder.stock.setTextColor(Color.RED);
        }
        holder.stock.setText(stock);
        holder.meta_description.setText(currentArticle.getMeta_description());
        final String descriptionFull = currentArticle.getDescription();

        //get thumbnail
        final String imageLarge = currentArticle.getThumb_large();
        final String image = currentArticle.getThumb_medium();
        Uri uri = Uri.parse(image);
        if (image != null && !image.isEmpty()) {
            Picasso.with(context)
                    .load(uri)
                    .into(holder.thumb);
        } else {
            holder.thumb.setImageResource(R.drawable.no_image);
        }

        //go to link on item click
        holder.article_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beginFragmentTransaction(ProductPresentationFragment.newInstance(name, imageLarge, descriptionFull, stock, price ));
            }
        });
    }
    private void beginFragmentTransaction(Fragment fragment){
        if (fragment != null) {
            android.support.v4.app.FragmentTransaction ft = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment).addToBackStack(null);
            ft.commit();
            ft.addToBackStack(null);
        }
    }
    public void addAll(List<Product> data) {
        productsList.clear();
        productsList.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        int items = this.productsList.size();
        return items;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.product_id)
        TextView product_id;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.imageView)
        ImageView thumb;
        @BindView(R.id.stock)
        TextView stock;
        @BindView(R.id.meta_description)
        TextView meta_description;
        @BindView(R.id.article_layout)
        View article_layout;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
