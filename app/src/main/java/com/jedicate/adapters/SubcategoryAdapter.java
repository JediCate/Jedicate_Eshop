package com.jedicate.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jedicate.models.Category;
import com.jedicate.sumireapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubcategoryAdapter extends RecyclerView.Adapter<SubcategoryAdapter.ViewHolder> {
    Context context;
    private final List<Category> subcategoryList;
    public CategoryAdapter.DataTransferInterface subcatData;

    public SubcategoryAdapter(Context context, List<Category> subcategoryList, CategoryAdapter.DataTransferInterface subcatData) {
        this.context = context;
        this.subcategoryList = subcategoryList;
        this.subcatData = subcatData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subcategory_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Category currentSubategory = subcategoryList.get(position);
        final String currentSubcatId = currentSubategory.getId();
        holder.subcat_name.setText(subcategoryList.get(position).getName());
        //get thumbnail
        String image = currentSubategory.getThumb_medium();
        Uri uri = Uri.parse(image);
        if (image != null && !image.isEmpty()) {
            Picasso.with(context)
                    .load(uri)
                    .into(holder.subcat_thumb);
        } else {
            holder.subcat_thumb.setImageResource(R.drawable.no_image);
        }
        //go to link on item click
        holder.subcat_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subcatData.getCategoryId(currentSubcatId);
            }
        });
    }
    public void addAll(List<Category> data) {
        subcategoryList.clear();
        subcategoryList.addAll(data);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return subcategoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.subcategory_name)
        TextView subcat_name;
        @BindView(R.id.subcat_imageView)
        ImageView subcat_thumb;
        @BindView(R.id.sub_cat_layout)
        View subcat_layout;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }
}
