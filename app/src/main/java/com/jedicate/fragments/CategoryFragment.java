package com.jedicate.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jedicate.adapters.CategoryAdapter;
import com.jedicate.loaders.CategoryLoader;
import com.jedicate.models.Category;
import com.jedicate.sumireapp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Category>>,CategoryAdapter.DataTransferInterface {
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    private static final String LOG_TAG = CategoryFragment.class.getName();

    private static final int LOADER_ID = 2;
    private String query = "";
    private static String searchQuery = "";
    public boolean searching = false;

    CategoryAdapter categoryAdapter;
    @BindView(R.id.cat_progressBar)
    ProgressBar catProgressBar;
    @BindView(R.id.cat_recycler_view)
    RecyclerView catRecyclerView;
    @BindView(R.id.cat_empty_tv)
    TextView cat_empty_tv;

    //only 60 and 65 have subcategories
    private static final String[] parentCategories = {"59","60","65","70","75"};
    private static final String READ_ALL_CATEGORIES = "http://www.creme-cosmetice-naturale.ro/index.php?route=api/categories/read";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_categories, container, false);
        ButterKnife.bind(this, fragmentView);
        setHasOptionsMenu(true);

        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Categories");
        catRecyclerView.setHasFixedSize(true);
        int screenWidth = getResources().getConfiguration().screenWidthDp;
        if (screenWidth <= 360) {//j5 portrait
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 360 && screenWidth <= 600) {//asus portrait
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 600 && screenWidth <= 800) {//j5 landscape
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 800) {//asus landscape
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        }

        catRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        RecyclerView.ItemAnimator animator = new DefaultItemAnimator();
        animator.setAddDuration(1000);
        animator.setRemoveDuration(1000);
        catRecyclerView.setItemAnimator(animator);


        categoryAdapter = new CategoryAdapter(getActivity(), new ArrayList<Category>(), this);
        catRecyclerView.setAdapter(categoryAdapter);

        //check network connectivity
        if (isOnline()) {
            query = READ_ALL_CATEGORIES;
            getLoaderManager().initLoader(LOADER_ID, null, this).forceLoad();
        } else {
            catProgressBar.setVisibility(View.GONE);
            cat_empty_tv.setVisibility(View.VISIBLE);
            cat_empty_tv.setText("Network not available");
        }
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    @Override
    public void getCategoryId(String catId) {
        if(catId.equals(parentCategories[1]) || catId.equals(parentCategories[2])){ // 60 & 65
            beginFragmentTransaction(SubcategoryFragment.newInstance(catId));
        }else{
            beginFragmentTransaction(ProductFragment.newInstance(catId));
        }
    }

    private void beginFragmentTransaction(Fragment fragment){
        if (fragment != null) {
            android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment).addToBackStack(null);
            ft.commit();
            ft.addToBackStack(null);
        }
    }

    @Override
    public Loader<List<Category>> onCreateLoader(int id, Bundle args) {
        if (searching) {
            searching = false;
            return new CategoryLoader(getActivity(), searchQuery);
        }
        return new CategoryLoader(getActivity(), query);
    }

    @Override
    public void onLoadFinished(Loader<List<Category>> loader, List<Category> data) {
        catProgressBar.setVisibility(View.GONE);
        if (data != null && !data.isEmpty()) {
            Log.v(LOG_TAG, "LOADING DATA IN ADAPTER");
            categoryAdapter.addAll(data);
            cat_empty_tv.setVisibility(View.GONE);
            catRecyclerView.setVisibility(View.VISIBLE);
        } else {
            catRecyclerView.setVisibility(View.GONE);
            cat_empty_tv.setVisibility(View.VISIBLE);
            cat_empty_tv.setText("Your search returned no results. \nDo you want to try again?");
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Category>> loader) {
        catRecyclerView.getRecycledViewPool().clear();
    }
}
