package com.jedicate.fragments;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jedicate.adapters.ProductsAdapter;
import com.jedicate.loaders.ProductsLoader;
import com.jedicate.models.Product;
import com.jedicate.sumireapp.MainActivity;
import com.jedicate.sumireapp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Product>> {
    StaggeredGridLayoutManager layoutManager;
    private static final String LOG_TAG = MainActivity.class.getName();
    private static final String CATEGORY_ID = "category_id";
    // TODO: Customize parameters
    private String category_id = "";
    private static final int LOADER_ID = 1;
    private String query = "";
    private static String searchQuery = "";
    public boolean searching = false;

    ProductsAdapter adapter;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.empty_tv)
    TextView empty_tv;

    private static final String READ_ALL_PRODUCTS = "http://www.creme-cosmetice-naturale.ro/index.php?route=api/products/read";
    private static final String READ_PRODUCTS_FOR_CATEGORY = "http://www.creme-cosmetice-naturale.ro/index.php?route=api/products/byCategory&path=";
    private static final String _ID = "http://www.creme-cosmetice-naturale.ro/index.php?route=api/products/readId&id=";
    private static final String SEARCH_URL = "http://www.creme-cosmetice-naturale.ro/index.php?route=api/products/search&";

    private static final String[] filters = {
            "sort", "search", "tag", "description", "category_id", "sub_category", "order", "page", "limit"};

    private int productId;

    public ProductFragment() {
        // Required empty public constructor
    }

    public static ProductFragment newInstance(String category_id) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putString(CATEGORY_ID, category_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            category_id = getArguments().getString(CATEGORY_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_products, container, false);
        ButterKnife.bind(this, fragmentView);
        setHasOptionsMenu(true);

        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Catalog");
        recyclerView.setHasFixedSize(true);
        int screenWidth = getResources().getConfiguration().screenWidthDp;
        if (screenWidth <= 360) {//j5 portrait
            layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 360 && screenWidth <= 600) {//asus portrait
            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 600 && screenWidth <= 800) {//j5 landscape
            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 800) {//asus landscape
            layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        }

        recyclerView.setLayoutManager(layoutManager);
        RecyclerView.ItemAnimator animator = new DefaultItemAnimator();
        animator.setAddDuration(1000);
        animator.setRemoveDuration(1000);
        recyclerView.setItemAnimator(animator);


        adapter = new ProductsAdapter(getActivity(), new ArrayList<Product>());
        recyclerView.setAdapter(adapter);

        //check network connectivity
        if (isOnline()) {
            if(getArguments() != null){
                query = READ_PRODUCTS_FOR_CATEGORY + category_id;
            }else{
                query = READ_ALL_PRODUCTS;
            }
            getLoaderManager().initLoader(LOADER_ID, null, this).forceLoad();
        } else {
            progressBar.setVisibility(View.GONE);
            empty_tv.setVisibility(View.VISIBLE);
            empty_tv.setText("Network not available");
        }
    }


    //NETWORK secton
    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    @Override
    public Loader<List<Product>> onCreateLoader(int id, Bundle args) {
        if (searching) {
            searching = false;
            return new ProductsLoader(getActivity(), searchQuery);
        }
        return new ProductsLoader(getActivity(), query);
    }

    @Override
    public void onLoadFinished(Loader<List<Product>> loader, List<Product> data) {
        progressBar.setVisibility(View.GONE);
        if (data != null && !data.isEmpty()) {
            Log.v(LOG_TAG, "LOADING DATA IN ADAPTER");
            adapter.addAll(data);
            empty_tv.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.GONE);
            empty_tv.setVisibility(View.VISIBLE);
            empty_tv.setText("Your search returned no results. \nDo you want to try again?");
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Product>> loader) {
        recyclerView.getRecycledViewPool().clear();
    }

    public void executeSearch(String query) {
        if (isOnline()) {
            searching = true;
            progressBar.setVisibility(View.VISIBLE);
            searchQuery = SEARCH_URL + query.replace(" ", "+").trim();

            Log.v(LOG_TAG, searchQuery);
            getLoaderManager().restartLoader(LOADER_ID, null, ProductFragment.this);
        } else {
            progressBar.setVisibility(View.GONE);
            empty_tv.setVisibility(View.VISIBLE);
            empty_tv.setText("Network not available");
        }
    }

}
