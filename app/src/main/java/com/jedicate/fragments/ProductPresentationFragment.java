package com.jedicate.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jedicate.sumireapp.R;

public class ProductPresentationFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String NAME = "name";
    private static final String IMAGE = "image";
    private static final String DESCRIPTION = "description";
    private static final String STOCK = "stock";
    private static final String PRICE = "price";
    FragmentTabHost fragmentTabHost;
    private static final int NUM_PAGES = 4;
    /**
     * The pager widget handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;
    /**
     * The pager adapter provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    // TODO: Rename and change types of parameters
    private String name;
    private String image;
    private String description;
    private String stock;
    private String price;

    public ProductPresentationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static ProductPresentationFragment newInstance(String name, String image,String description,String stock, String price) {
        ProductPresentationFragment fragment = new ProductPresentationFragment();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        args.putString(IMAGE, image);
        args.putString(DESCRIPTION, description);
        args.putString(STOCK, stock);
        args.putString(PRICE, price);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME);
            image = getArguments().getString(IMAGE);
            description = getArguments().getString(DESCRIPTION);
            stock = getArguments().getString(STOCK);
            price = getArguments().getString(PRICE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_product_presentation, container, false);

        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mPager);
        FloatingActionButton cartFab = rootView.findViewById(R.id.cart_fab);
        cartFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity().getApplicationContext(), "Added product to cart", Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }
    /**
     * A simple pager adapter that represents 3 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    TabMainFragment tab1 = TabMainFragment.newInstance(name, image, stock, price);
                    return tab1;
                case 1:
                    TabSpecsFragment tab2 = new TabSpecsFragment();
                    return tab2;
                case 2:
                    TabDescriptionFragment tab3 = TabDescriptionFragment.newInstance(description);
                    return tab3;
                case 3:
                    TabReviewFragment tab4 = new TabReviewFragment();
                    return tab4;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "PRODUS";
                case 1:
                    return "SPECIFICATII";
                case 2:
                    return "DESCRIERE";
                case 3:
                    return "REVIEW-URI";
            }
            return null;
        }
    }

}
