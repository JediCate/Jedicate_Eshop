package com.jedicate.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jedicate.adapters.CategoryAdapter;
import com.jedicate.adapters.SubcategoryAdapter;
import com.jedicate.loaders.CategoryLoader;
import com.jedicate.models.Category;
import com.jedicate.sumireapp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SubcategoryFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Category>>, CategoryAdapter.DataTransferInterface{
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    private static final String LOG_TAG = SubcategoryFragment.class.getName();

    private static final int LOADER_ID = 3;
    private String query = "";
    private static String searchQuery = "";
    public boolean searching = false;

    SubcategoryAdapter subcategoryAdapter;
    @BindView(R.id.subcat_progressBar)
    ProgressBar subcatProgressBarr;
    @BindView(R.id.subcat_recycler_view)
    RecyclerView subcatRecyclerView;
    @BindView(R.id.subcat_empty_tv)
    TextView subcat_empty_tv;

    // TODO: Customize parameter argument names
    private static final String PARENT_CATEGORY_ID = "parent_id";
    // TODO: Customize parameters
    private String parent_id = "";
    private static final String SUBCATEGORIES = "http://www.creme-cosmetice-naturale.ro/index.php?route=api/categories/read&path=";

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SubcategoryFragment() {

    }

    @SuppressWarnings("unused")
    public static SubcategoryFragment newInstance(String parent_id) {
        SubcategoryFragment fragment = new SubcategoryFragment();
        Bundle args = new Bundle();
        args.putString(PARENT_CATEGORY_ID, parent_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            parent_id = getArguments().getString(PARENT_CATEGORY_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentView = inflater.inflate(R.layout.fragment_subcategory, container, false);
        ButterKnife.bind(this, fragmentView);
        setHasOptionsMenu(true);

        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Categories");
        subcatRecyclerView.setHasFixedSize(true);
        int screenWidth = getResources().getConfiguration().screenWidthDp;
        if (screenWidth <= 360) {//j5 portrait
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 360 && screenWidth <= 600) {//asus portrait
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 600 && screenWidth <= 800) {//j5 landscape
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 800) {//asus landscape
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        }

        subcatRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        RecyclerView.ItemAnimator animator = new DefaultItemAnimator();
        animator.setAddDuration(1000);
        animator.setRemoveDuration(1000);
        subcatRecyclerView.setItemAnimator(animator);


        subcategoryAdapter = new SubcategoryAdapter(getActivity(), new ArrayList<Category>(), this);
        subcatRecyclerView.setAdapter(subcategoryAdapter);

        //check network connectivity
        if (isOnline()) {
            query = SUBCATEGORIES + parent_id;
            getLoaderManager().initLoader(LOADER_ID, null, this).forceLoad();
        } else {
            subcatProgressBarr.setVisibility(View.GONE);
            subcat_empty_tv.setVisibility(View.VISIBLE);
            subcat_empty_tv.setText("Network not available");
        }
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    @Override
    public Loader<List<Category>> onCreateLoader(int id, Bundle args) {
        if (searching) {
            searching = false;
            return new CategoryLoader(getActivity(), searchQuery);
        }
        return new CategoryLoader(getActivity(), query);
    }

    @Override
    public void onLoadFinished(Loader<List<Category>> loader, List<Category> data) {
        subcatProgressBarr.setVisibility(View.GONE);
        if (data != null && !data.isEmpty()) {
            Log.v(LOG_TAG, "LOADING DATA IN ADAPTER");
            subcategoryAdapter.addAll(data);
            subcat_empty_tv.setVisibility(View.GONE);
            subcatRecyclerView.setVisibility(View.VISIBLE);
        } else {
            subcatRecyclerView.setVisibility(View.GONE);
            subcat_empty_tv.setVisibility(View.VISIBLE);
            subcat_empty_tv.setText("Your search returned no results. \nDo you want to try again?");
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Category>> loader) {
        subcatRecyclerView.getRecycledViewPool().clear();
    }

    @Override
    public void getCategoryId(String catId) {
        beginFragmentTransaction(ProductFragment.newInstance(catId));
    }

    private void beginFragmentTransaction(Fragment fragment){
        if (fragment != null) {
            android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment).addToBackStack(null);
            ft.commit();
            ft.addToBackStack(null);
        }
    }
}
