package com.jedicate.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jedicate.sumireapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TabDescriptionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String DESCRIPTION = "description";
    private String description;
    @BindView(R.id.tab_prod_description)
    TextView tabDescription;

    public TabDescriptionFragment() {
        // Required empty public constructor
    }


    public static TabDescriptionFragment newInstance(String description) {
        TabDescriptionFragment fragment = new TabDescriptionFragment();
        Bundle args = new Bundle();
        args.putString(DESCRIPTION, description);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            description = getArguments().getString(DESCRIPTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tab_description, container, false);
        ButterKnife.bind(this, rootView);
        tabDescription.setText(description);
        return rootView;
    }
}
