package com.jedicate.fragments;


import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jedicate.sumireapp.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TabMainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabMainFragment extends Fragment {
    private static final String NAME = "name";
    private static final String IMAGE = "image";
    private static final String STOCK = "stock";
    private static final String PRICE = "price";

    private String name;
    private String image;
    private String stock;
    private String price;

    @BindView(R.id.tab_prod_name)
    TextView tabName;
    @BindView(R.id.tab_prod_image)
    ImageView tabImage;
    @BindView(R.id.tab_prod_stock)
    TextView tabStock;
    @BindView(R.id.tab_prod_price)
    TextView tabPrice;
    @BindView(R.id.ratingBar)
    RatingBar tabRatingBar;


    public TabMainFragment() {
        // Required empty public constructor
    }

    public static TabMainFragment newInstance(String name, String image, String stock, String price) {
        TabMainFragment fragment = new TabMainFragment();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        args.putString(IMAGE, image);
        args.putString(STOCK, stock);
        args.putString(PRICE, price);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME);
            image = getArguments().getString(IMAGE);
            stock = getArguments().getString(STOCK);
            price = getArguments().getString(PRICE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tab_main, container, false);
        ButterKnife.bind(this, rootView);
        tabName.setText(name);
        tabStock.setText(stock);
        tabPrice.setText(price);

        Uri uri = Uri.parse(image);
        if (image != null && !image.isEmpty()) {
            Picasso.with(getActivity().getApplicationContext())
                    .load(uri)
                    .into(tabImage);
        } else {
            tabImage.setImageResource(R.drawable.no_image);
        }
        tabRatingBar.setEnabled(true);
        tabRatingBar.setClickable(true);
        tabRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                Toast.makeText(getActivity().getApplicationContext(), "Your rating is: " + String.valueOf(ratingBar.getRating()), Toast.LENGTH_SHORT ).show();
            }
        });
        tabRatingBar.refreshDrawableState();

        return rootView;
    }

}
