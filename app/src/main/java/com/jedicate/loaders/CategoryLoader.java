package com.jedicate.loaders;

import android.app.Activity;

import com.jedicate.models.Category;
import com.jedicate.utils.CategoryQueryUtils;

import java.util.List;

/**
 * Created by Trompetica on 1/25/2018.
 */

public class CategoryLoader extends android.support.v4.content.AsyncTaskLoader<List<Category>> {

    private String url;

    public CategoryLoader(Activity activity, String url) {
        super(activity);
        this.url = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<Category > loadInBackground() {
        if (url == null) {
            return null;
        }
        return CategoryQueryUtils.fetchData(url);
    }

}