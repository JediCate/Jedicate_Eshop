package com.jedicate.loaders;

import android.app.Activity;

import com.jedicate.models.Product;
import com.jedicate.utils.ProductsQueryUtils;

import java.util.List;

/**
 * Created by Trompetica on 1/22/2018.
 */

public class ProductsLoader extends android.support.v4.content.AsyncTaskLoader<List<Product>> {

    private String url;

    public ProductsLoader(Activity activity, String url) {
        super(activity);
        this.url = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<Product > loadInBackground() {
        if (url == null) {
            return null;
        }
        return ProductsQueryUtils.fetchData(url);
    }

}
