package com.jedicate.models;

/**
 * Created by Trompetica on 1/25/2018.
 */

public class Category {
    private String id;
    private String name;
    private String description;
    private String image;
    private String thumb_small;
    private String thumb_medium;
    private String thumb_large;

    public Category(){}

    public Category(String id, String name, String description, String image,String thumb_small, String thumb_medium, String thumb_large){
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.thumb_small = thumb_small;
        this.thumb_medium = thumb_medium;
        this.thumb_large = thumb_large;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public String getThumb_small() {
        return thumb_small;
    }

    public String getThumb_medium() {
        return thumb_medium;
    }

    public String getThumb_large() {
        return thumb_large;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", thumb_small='" + thumb_small + '\'' +
                ", thumb_medium='" + thumb_medium + '\'' +
                ", thumb_large='" + thumb_large + '\'' +
                '}';
    }
}
