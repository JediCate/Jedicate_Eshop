package com.jedicate.models;

/**
 * Created by Trompetica on 1/22/2018.
 */

public class Product {
    private String product_id;
    private String name;
    private String description;
    private String meta_description;
    private String stock;
    private String image;
    private String thumb_small;
    private String thumb_medium;
    private String thumb_large;

    private String price;

    public Product(){ }

    public Product(String product_id, String name, String description, String meta_description, String stock, String image, String price, String thumb_small, String thumb_medium, String thumb_large){
        this.product_id = product_id;
        this.name = name;
        this.description = description;
        this.meta_description = meta_description;
        this.stock = stock;
        this.image = image;
        this.price = price;
        this.thumb_small = thumb_small;
        this.thumb_medium = thumb_medium;
        this.thumb_large = thumb_large;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getMeta_description() {
        return meta_description;
    }

    public String getStock() {
        return stock;
    }

    public String getImage() {
        return image;
    }

    public String getPrice() {
        return price;
    }

    public String getThumb_small() {
        return thumb_small;
    }

    public String getThumb_medium() {
        return thumb_medium;
    }

    public String getThumb_large() {
        return thumb_large;
    }

    @Override
    public String toString() {
        return "Product{" +
                "product_id='" + product_id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", meta_description='" + meta_description + '\'' +
                ", stock='" + stock + '\'' +
                ", image='" + image + '\'' +
                ", thumb_small='" + thumb_small + '\'' +
                ", thumb_medium='" + thumb_medium + '\'' +
                ", thumb_large='" + thumb_large + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
