package com.jedicate.sumireapp;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.provider.SearchRecentSuggestions;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;

import com.jedicate.fragments.AccountFragment;
import com.jedicate.fragments.CartFragment;
import com.jedicate.fragments.CategoryFragment;
import com.jedicate.fragments.FavoritesFragment;
import com.jedicate.fragments.HomePageFragment;
import com.jedicate.fragments.ProductFragment;
import com.jedicate.utils.SearchSuggestionProvider;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    @BindView(R.id.nav_view)
    public NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    public DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        if(savedInstanceState == null){
            beginFragmentTransaction(new HomePageFragment(), R.id.nav_home);
        }
        handleIntent(getIntent());
    }

   //search section
    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if(Intent.ACTION_SEARCH.equals(intent.getAction())){
            String query = intent.getStringExtra(SearchManager.QUERY);
            //save query history
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);
            new ProductFragment().executeSearch(query);
        }
    }

    //general handling
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (fragmentManager.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fragmentManager.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);

        return true;
    }

    private void beginFragmentTransaction(Fragment fragment, int id){
        navigationView.setCheckedItem(id);
        if (fragment != null) {
            android.support.v4.app.FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.content_frame, fragment).addToBackStack(null);
            ft.commit();
            ft.addToBackStack(null);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //noinspection SimplifiableIfStatement
        switch (itemId){
            case R.id.action_search:
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                return true;
            case R.id.action_favorite:
                beginFragmentTransaction(new FavoritesFragment(), R.id.nav_favorite);
                return true;
            case R.id.action_cart:
                beginFragmentTransaction(new CartFragment(), R.id.nav_cart);
                return true;
            case R.id.action_settings:
                Intent notifications = new Intent(this, SettingsActivity.class);
                notifications.putExtra( PreferenceActivity.EXTRA_SHOW_FRAGMENT, SettingsActivity.NotificationPreferenceFragment.class.getName() );
                notifications.putExtra( PreferenceActivity.EXTRA_NO_HEADERS, true );
                startActivity(notifications);
                return true;
        }        //replacing the fragment


        drawer.closeDrawer(GravityCompat.START);

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if(item.isChecked()){
            beginFragmentTransaction(new HomePageFragment(),R.id.nav_home);
        }

        //initializing the fragment object which is selected
        switch (itemId){
            case R.id.nav_home:
                beginFragmentTransaction(new HomePageFragment(),R.id.nav_home);
                break;
            case R.id.nav_produse:
                beginFragmentTransaction(new CategoryFragment(),R.id.nav_produse);
                break;
            case R.id.nav_favorite:
                beginFragmentTransaction(new FavoritesFragment(),R.id.nav_favorite);
                break;
            case R.id.nav_cart:
                beginFragmentTransaction(new CartFragment(),R.id.nav_cart);
                break;
            case R.id.nav_account:
                beginFragmentTransaction(new AccountFragment(),R.id.nav_account);
                break;
            case R.id.nav_share:
                break;
            case R.id.nav_notif:
                Intent notifications = new Intent(this, SettingsActivity.class);
                notifications.putExtra( PreferenceActivity.EXTRA_SHOW_FRAGMENT, SettingsActivity.NotificationPreferenceFragment.class.getName() );
                notifications.putExtra( PreferenceActivity.EXTRA_NO_HEADERS, true );
                startActivity(notifications);
                break;
            case R.id.nav_delivery:
                break;
            case R.id.nav_contact:
                break;
            default:
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
