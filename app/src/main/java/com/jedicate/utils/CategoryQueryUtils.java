package com.jedicate.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.jedicate.models.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trompetica on 1/25/2018.
 */

public class CategoryQueryUtils {
    private static final String LOG_TAG = CategoryQueryUtils.class.getName();
    private static Context context;

    public CategoryQueryUtils(Context context) {
        this.context = context;
    }

    public static List<Category> fetchData(String stringUrl) {
        URL url = createURL(stringUrl);
        String jsonResponse = "";
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "HTTP Request error " + e.getMessage());
        }

        List<Category> categoryListList = extractCategoryData(jsonResponse);
        return categoryListList;
    }

    private static URL createURL(String stringURL) {
        URL url = null;
        try {
            url = new URL(stringURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Create url error: ", e.getCause());
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readInputStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Response CODE is --> " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readInputStream(InputStream input) throws IOException {
        StringBuilder output = new StringBuilder();
        if (input != null) {
            InputStreamReader inputReader = new InputStreamReader(input, Charset.forName("UTF-8"));
            BufferedReader buffer = new BufferedReader(inputReader);
            String line = buffer.readLine();
            while (line != null) {
                output.append(line);
                line = buffer.readLine();
            }
        }
        return output.toString();
    }

    public static List<Category> extractCategoryData(String jsonResponse) {
        if (TextUtils.isEmpty(jsonResponse)) {
            return null;
        }
        List<Category> Categories = new ArrayList<>();
        try {
            JSONObject root = new JSONObject(jsonResponse);
            JSONObject success = root.getJSONObject("success");
            JSONArray categories = new JSONArray();
            if (success.has("categories")) {
                categories = success.getJSONArray("categories");
            }

            for (int i= 0; i < categories.length(); i++) {
                String id="";
                String name="";
                String description="";
                String image="";
                JSONObject cat = categories.getJSONObject(i);
                if(cat.has("category_id")){
                    id = cat.getString("category_id");
                }
                if(cat.has("name")){
                    name = cat.getString("name");
                }
                if(cat.has("description")){
                    description = cat.getString("description");
                }
                if(cat.has("image")){
                    image = cat.getString("image");
                }
                String thumb_small = "";
                if (cat.has("thumb_small")) {
                    thumb_small = cat.getString("thumb_small");
                }

                String thumb_medium = "";
                if (cat.has("thumb_medium")) {
                    thumb_medium = cat.getString("thumb_medium");
                }

                String thumb_large = "";
                if (cat.has("thumb_large")) {
                    thumb_large = cat.getString("thumb_large");
                }


                Category item = new Category(id, name, description, image, thumb_small, thumb_medium, thumb_large);
                Categories.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Categories;
    }
}
