package com.jedicate.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.jedicate.models.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trompetica on 1/22/2018.
 */

public class ProductsQueryUtils {
    private static final String LOG_TAG = ProductsQueryUtils.class.getName();
    private static Context context;

    public ProductsQueryUtils(Context context) {
        this.context = context;
    }

    public static List<Product> fetchData(String stringUrl) {
        URL url = createURL(stringUrl);
        String jsonResponse = "";
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "HTTP Request error " + e.getMessage());
        }

        List<Product> productList = extractProductData(jsonResponse);
        return productList;
    }

    private static URL createURL(String stringURL) {
        URL url = null;
        try {
            url = new URL(stringURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Create url error: ", e.getCause());
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readInputStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Response CODE is --> " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readInputStream(InputStream input) throws IOException {
        StringBuilder output = new StringBuilder();
        if (input != null) {
            InputStreamReader inputReader = new InputStreamReader(input, Charset.forName("UTF-8"));
            BufferedReader buffer = new BufferedReader(inputReader);
            String line = buffer.readLine();
            while (line != null) {
                output.append(line);
                line = buffer.readLine();
            }
        }
        return output.toString();
    }

    public static List<Product> extractProductData(String jsonResponse) {
        if (TextUtils.isEmpty(jsonResponse)) {
            return null;
        }
        List<Product> Products = new ArrayList<>();
        try {
            JSONObject root = new JSONObject(jsonResponse);
            JSONObject success = root.getJSONObject("success");
            JSONArray products = new JSONArray();
            if (success.has("products")) {
                products = success.getJSONArray("products");
            }
            for(int i= 0; i < products.length(); i++){
                Object thisProduct = products.get(i);
                if (thisProduct instanceof JSONObject ) {
                    JSONObject currentProduct = (JSONObject) thisProduct;
                    String name = currentProduct.getString("name");

                    String description = "";
                    if (currentProduct.has("description")) {
                        description = currentProduct.getString("description");
                    }

                    String meta_description = "";
                    if (currentProduct.has("meta_description")) {
                        meta_description = currentProduct.getString("meta_description");
                    }

                    String price = "";
                    if (currentProduct.has("price")) {
                        price = currentProduct.getString("price");
                    }

                    String image = "";
                    if (currentProduct.has("image")) {
                        description = currentProduct.getString("image");
                    }

                    String stock = "";
                    if (currentProduct.has("stock")) {
                        stock = currentProduct.getString("stock");
                    }

                    String id = "";
                    if (currentProduct.has("product_id")) {
                        id = currentProduct.getString("product_id");
                    }

                    String thumb_small = "";
                    if (currentProduct.has("thumb_small")) {
                        thumb_small = currentProduct.getString("thumb_small");
                    }

                    String thumb_medium = "";
                    if (currentProduct.has("thumb_medium")) {
                        thumb_medium = currentProduct.getString("thumb_medium");
                    }

                    String thumb_large = "";
                    if (currentProduct.has("thumb_large")) {
                        thumb_large = currentProduct.getString("thumb_large");
                    }
                    Product item = new Product(id, name, description, meta_description, stock, image, price, thumb_small, thumb_medium, thumb_large);
                    Products.add(item);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Products;
    }

}
